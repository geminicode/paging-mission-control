package com.systolic.bdp.utils;

import java.time.Instant;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.systolic.bdp.dto.SatelliteAlertDto;
import com.systolic.bdp.model.SatelliteData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SatelliteMapperTest {

	@Test
	void testNotNull() {
		assertNotNull(new SatelliteMapper());
	}
	
	@Test
	void testConvertToDto() {
		SatelliteData alert = getSatelliteAlert();
		SatelliteAlertDto dto = SatelliteMapper.convertToDto(alert);
		assertNotNull(dto);
		assertEquals(alert.getComponent(), dto.getComponent());
		assertEquals(alert.getSatelliteId(), dto.getSatelliteId());
		assertEquals(alert.getTimestamp(), dto.getTimestamp());
	}

	@Test
	void testConvertToJson() throws JsonProcessingException {
		SatelliteData alert = getSatelliteAlert();
		SatelliteAlertDto dto = SatelliteMapper.convertToDto(alert);
		dto.setSeverity(SatelliteAlertDto.RED_HIGH);
		String jsonString = SatelliteMapper.convertToJson(dto);
		assertNotNull(jsonString);
	}

	@Test
	void testConvertToJsonArray() throws JsonProcessingException {
		SatelliteData alert = getSatelliteAlert();
		SatelliteAlertDto dto = SatelliteMapper.convertToDto(alert);
		SatelliteAlertDto dto2 = SatelliteMapper.convertToDto(alert);
		dto.setSeverity(SatelliteAlertDto.RED_HIGH);
		dto2.setSeverity(SatelliteAlertDto.RED_LOW);
		dto2.setSatelliteId(1002);
		List<SatelliteAlertDto> list = List.of(dto, dto2);
		SatelliteMapper.printToJson(list);
	}
	
	private SatelliteData getSatelliteAlert() {
		return SatelliteData.builder()
			.timestamp(Instant.now())
			.satelliteId(1001)
			.redHigh(17)
			.redLow(15)
			.yellowHigh(9)
			.yellowLow(8)
			.value(7.9)
			.component("BATT")
			.build();
	}
}
