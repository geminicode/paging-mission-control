package com.systolic.bdp.model;

import java.time.Instant;
import org.junit.jupiter.api.Test;
import com.systolic.bdp.dto.SatelliteAlertDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SatelliteDataTest {

	@Test
	void defaultConstructor() {
		assertNotNull(new SatelliteData());
	}

	@Test
	void testModelSetters() {
		SatelliteData data = getData();
		SatelliteData data2 = getData();
		SatelliteData data3 = new SatelliteData();
		
		assertTrue(data.equals(data));
		assertFalse(data.equals(data2));
		assertFalse(data.equals(data3));
		assertFalse(data.equals(null));
		assertNotNull(data.toString());
		assertNotNull(data.hashCode());
		assertEquals(data.getYellowHigh(), data2.getYellowHigh());
		assertEquals(data.getYellowLow(), data2.getYellowLow());
	}

	@Test
	void testModelBuilder() {
		SatelliteData data = getData();
		SatelliteData data2 = SatelliteData.builder()
			.component(data.getComponent())
			.redHigh(data.getRedHigh())
			.redLow(data.getRedLow())
			.yellowHigh(data.getYellowHigh())
			.yellowLow(data.getYellowLow())
			.satelliteId(data.getSatelliteId())
			.value(data.getValue())
			.build();
		assertFalse(data.equals(data2));
		assertNotNull(SatelliteData.builder().toString());
	}
	
	private SatelliteData getData() {
		SatelliteData data = new SatelliteData();
		data.setComponent(SatelliteAlertDto.BATTERY);
		data.setRedHigh(10);
		data.setRedLow(5);
		data.setYellowHigh(90);
		data.setYellowLow(70);
		data.setTimestamp(Instant.now());
		data.setSatelliteId(1001);
		data.setValue(85.0);
		return data;
	}
}
