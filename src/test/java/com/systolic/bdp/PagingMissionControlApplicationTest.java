package com.systolic.bdp;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PagingMissionControlApplicationTest {

	@Autowired
	PagingMissionControlApplication application;
	
	@Test
	void testNotNull() {
		assertNotNull(application);
	}
	
	@Test
	void testMainHelp() {
		PagingMissionControlApplication.main(new String[] {"--help"});
		assertTrue(true);
	}

	@Test
	void testRun() {
		PagingMissionControlApplication.main(new String[] {"src/test/resources/test-data.txt"});
		assertTrue(true);
	}
}