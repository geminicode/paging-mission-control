package com.systolic.bdp.service;

import java.io.IOException;
import java.util.List;
import java.util.TimeZone;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.systolic.bdp.io.TelemetryFileProcessor;
import com.systolic.bdp.model.SatelliteData;

public class AlertServiceTest {

	AlertService service = new AlertService();
	
	@BeforeAll
	static void setup() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	
	@Test
	void processAllAlerts() throws IOException {
		TelemetryFileProcessor processor = new TelemetryFileProcessor();
		List<SatelliteData> alerts = processor.parseFile("src/test/resources/test-data2.txt");
		service.processAllAlerts(alerts);
	}
}
