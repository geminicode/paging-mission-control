package com.systolic.bdp.io;

import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;
import com.systolic.bdp.model.SatelliteData;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TelemetryFileProcessorTest {

	@Test
	void readTestFile() throws IOException {
		TelemetryFileProcessor processor = new TelemetryFileProcessor();
		List<SatelliteData> alerts = processor.parseFile("src/test/resources/test-data.txt");
		assertEquals(14, alerts.size());
	}

	@Test
	void readTestFileNumberFormatException() throws IOException {
		TelemetryFileProcessor processor = new TelemetryFileProcessor();
		List<SatelliteData> alerts = processor.parseFile("src/test/resources/test-data-nfe.bad.txt");
		assertEquals(11, alerts.size());
	}
}
