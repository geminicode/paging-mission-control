/**
 * file: PagingMissionControlApplication.java
 * 
 * SpringBoot Console application.
 */
package com.systolic.bdp;

import java.io.IOException;
import java.util.List;
import java.util.TimeZone;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.systolic.bdp.dto.SatelliteAlertDto;
import com.systolic.bdp.io.TelemetryFileProcessor;
import com.systolic.bdp.model.SatelliteData;
import com.systolic.bdp.service.AlertService;
import com.systolic.bdp.utils.SatelliteMapper;

@SpringBootApplication
public class PagingMissionControlApplication implements CommandLineRunner {

	/**
	 * Java Main as indicated by manifest
	 * @param args passed during startup
	 */
    public static void main(String[] args) {
    	// Set Timezone to UTC to match timestamps in datafile.
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(PagingMissionControlApplication.class, args);
    }
 
    /**
     * How to use this app from the command line
     */
    private void usage() {
    	System.out.println("********** Usage ****************");
    	System.out.println("Process a Telementary File for Alerts");
    	System.out.println("Usage: java -jar pgm-1.0.0-SNAPSHOT.jar <path/to/telementary.file>");
    	System.out.println("*********************************\n");
    }
    
    /**
     * Executes after spring has initialized the application.
     */
    @Override
    public void run(String... args) {
        
        if (args.length != 1) {
        	usage();
        } else {
        	TelemetryFileProcessor processor = new TelemetryFileProcessor();
			try {
				List<SatelliteData> data = processor.parseFile(args[0]);
	    		AlertService service = new AlertService();
	    		List<SatelliteAlertDto> alerts = service.processAllAlerts(data);
	    		SatelliteMapper.printToJson(alerts);
			} catch (IOException e) {
				System.out.println(String.format("Error Processing %s", args[0]));
			}
        }
    }
}