package com.systolic.bdp.model;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SatelliteData {

	/**
	 * format: 20180101 23:01:05.001
	 */
	Instant timestamp;
	int satelliteId;
	int redHigh;
	int redLow;
	int yellowHigh;
	int yellowLow;
	double value;
	String component;
}
