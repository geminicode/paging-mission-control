package com.systolic.bdp.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import com.systolic.bdp.dto.SatelliteAlertDto;
import com.systolic.bdp.model.SatelliteData;
import com.systolic.bdp.utils.SatelliteMapper;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class AlertService {
	
	private static final int FIVE_MINUTES = 5 * 60;

	public List<SatelliteAlertDto> processAllAlerts(List<SatelliteData> alerts) {
		Map<Integer, List<SatelliteData>> satAlerts = new HashMap<>();
		for (SatelliteData alert : alerts) {
			int key = alert.getSatelliteId();
			if (!satAlerts.containsKey(key)) {
				satAlerts.put(key, new ArrayList<SatelliteData>());
			}
			satAlerts.get(key).add(alert);
		}
		
		List<SatelliteAlertDto> allAlerts = new ArrayList<>();
		satAlerts.keySet().forEach(satellite -> {
			List<SatelliteData> list = satAlerts.get(satellite);
			log.debug(String.format("Processing %s alerts for Satellite[%s]", list.size(), satellite));
			allAlerts.addAll(processSatelliteAlerts(list));
		});
		return allAlerts;
	}
	
	/**
	 * 
	 * If for the same satellite there are three battery voltage readings 
	 * that are under the red low limit within a five minute interval.
     * 
     * If for the same satellite there are three thermostat readings that
     * exceed the red high limit within a five minute interval.
	 * @param alerts
	 */
	public List<SatelliteAlertDto> processSatelliteAlerts(List<SatelliteData> alerts) {
		List<SatelliteAlertDto> redHighAlerts = new ArrayList<>();
		List<SatelliteAlertDto> redLowAlerts = new ArrayList<>();
		
		// Don't assume the list is in date order
		alerts.sort(Comparator.comparing(SatelliteData::getTimestamp));
		
		for (SatelliteData alert : alerts) {
			String component = alert.getComponent();
			double value = alert.getValue();
			boolean highTemp = (value > alert.getRedHigh()) && SatelliteAlertDto.TERMOSTAT.equals(component);
			boolean lowBattery = (value < alert.getRedLow()) && SatelliteAlertDto.BATTERY.equals(component);
			if (highTemp) {
				addAlert(redHighAlerts, alert, SatelliteAlertDto.RED_HIGH);
			}
			if (lowBattery) {
				addAlert(redLowAlerts, alert, SatelliteAlertDto.RED_LOW);
			}
		}
		// Combine the lists
		List<SatelliteAlertDto> allAerts = new ArrayList<>(redHighAlerts);
		allAerts.addAll(redLowAlerts);
		
		// Filter alerts that occurred less than three (3) times
		allAerts = allAerts.stream().filter((Predicate<SatelliteAlertDto>)(alert) -> { return alert.getCount() >= 3; }).collect(Collectors.toList());

		allAerts.forEach(alert -> {
			log.debug(String.format("[%s] [%s] #%s %s %s ", 
					alert.getSatelliteId(), 
					alert.getTimestamp(),
					alert.getCount(),
					alert.getSeverity(),
					alert.getComponent())
				);
		});

		return allAerts;
	}
	
	/**
	 * Process Alerts for three (3) occurrences of the same alert within
	 * a five (5) minutes window
	 * NOTE: List should ALWAYS be in date order (@see processSatelliteAlerts) 
	 * @param list of alerts to add to.
	 * @param alert to add (conditionally)
	 * @param severity <RED HIGH | RED LOW>
	 */
	private void addAlert(List<SatelliteAlertDto> list, SatelliteData alert, String severity) {
		SatelliteAlertDto dto = SatelliteMapper.convertToDto(alert);
		dto.setSeverity(severity);
		dto.setCount(1);
		boolean found = false;
		// Iterate through all the alerts trying to find a match based on time
		for (SatelliteAlertDto item : list) {
			// Do checks here
			Instant endTime = item.getTimestamp().plusSeconds(FIVE_MINUTES);
			Instant compareTime = dto.getTimestamp();
			
			// is the time within 5 min?
			if (compareTime.isBefore(endTime)) {
				item.setCount(item.getCount()+1);
				found = true;
			}
		}
		
		if (!found) {
			list.add(dto);
		}
	}
}
