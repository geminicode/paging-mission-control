package com.systolic.bdp.utils;

import java.util.Arrays;
import java.util.List;
import org.modelmapper.ModelMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.systolic.bdp.dto.SatelliteAlertDto;
import com.systolic.bdp.model.SatelliteData;

/**
 * Mapper to convert Objects to JSON and Models to DTO and vice versa 
 *
 */
public class SatelliteMapper {

	private static ModelMapper modelMapper = new ModelMapper();
	private static ObjectMapper objectMapper;
	
	/**
	 * Helper to customize a ModelMapper
	 * NOTE: No customization at this time.
	 * @return ModelMapper
	 */
	private static ModelMapper getModelMapper() { 
		return modelMapper;
	}
	
	/**
	 * Helper to customize an Object Mapper
	 * @return ObjectMapper
	 */
	private static ObjectMapper getObjectMapper() {
		if (objectMapper == null) {
			objectMapper = new ObjectMapper();
			objectMapper.registerModule(new JavaTimeModule());
			objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);	
		}
		return objectMapper;
	}
	
	/**
	 * Convert an Alert to JSON
	 * @param satelliteAlertDto
	 * @return JSON formatted Array of Alert List
	 * @throws JsonProcessingException
	 */
	public static String convertToJson(SatelliteAlertDto satelliteAlertDto) throws JsonProcessingException {
		return convertToJson(Arrays.asList(satelliteAlertDto));
	}

	/**
	 * Convert an Alert List to JSON
	 * @param satelliteAlertList
	 * @return JSON formatted Array of Alert List
	 * @throws JsonProcessingException
	 */
	public static String convertToJson(List<SatelliteAlertDto> satelliteAlertList) throws JsonProcessingException {
		return getObjectMapper().writeValueAsString(satelliteAlertList);
	}

	/**
	 * Convert Model Object to a Dto
	 * @param satelliteAlert model to convert
	 * @return SatelliteAlertDto converted object
	 */
	public static SatelliteAlertDto convertToDto(SatelliteData satelliteAlert) {
		return getModelMapper().map(satelliteAlert, SatelliteAlertDto.class);
	}

	/**
	 * JSON Pretty Print Alert List to Screen 
	 * @param satelliteAlertList to print
	 * @throws JsonProcessingException if parsing JSON
	 */
	public static void printToJson(List<SatelliteAlertDto> satelliteAlertList) throws JsonProcessingException {
		String jsonString = getObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(satelliteAlertList);
		System.out.println(jsonString);
	}
}
