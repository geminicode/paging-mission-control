package com.systolic.bdp.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import com.systolic.bdp.model.SatelliteData;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TelemetryFileProcessor {

	private static String FIELD_DELIMETER="\\|";
	
	enum AlertField {
		TIMESTAMP,
		SATELLITE_ID,
		RED_HIGH_LIMIT,
		YELLOW_HIGH_LIMIT,
		YELLOW_LOW_LIMIT,
		RED_LOW_LIMIT,
		VALUE,
		COMPONENT;
		
		@Override
		public String toString() {
			return super.toString().toLowerCase().replace("_", "-");
		}
	}
	
	/**
	 * Alert File format
	 * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
	 * @param filename of alert file
	 * @return List of SatelliteAlerts
	 * @throws IOException if file is not found
	 */
	public List<SatelliteData> parseFile(String filename) throws IOException {
		List<SatelliteData> alerts = new ArrayList<>();
		try (Stream<String> lines = Files.lines(Paths.get(filename))) {
			lines.forEach(line -> {
				String[] data =line.split(FIELD_DELIMETER);
				try {
					SatelliteData alert = SatelliteData.builder()
							.timestamp(parseDateTime(data[AlertField.TIMESTAMP.ordinal()]))
							.satelliteId(parseInt(data, AlertField.SATELLITE_ID))
							.redHigh(parseInt(data, AlertField.RED_HIGH_LIMIT))
							.yellowHigh(parseInt(data, AlertField.YELLOW_HIGH_LIMIT))
							.yellowLow(parseInt(data, AlertField.YELLOW_LOW_LIMIT))
							.redLow(parseInt(data, AlertField.RED_LOW_LIMIT))
							.value(parseDouble(data, AlertField.VALUE))
							.component(data[AlertField.COMPONENT.ordinal()])
							.build();
					alerts.add(alert);
				} catch (NumberFormatException | ParseException e) {
					log.error(String.format("%s", e.getMessage()));  
				}
			});
		}
		
		return alerts;
	}

	/**
	 * Format: 20180101 23:05:07.421
	 *         20180101 23:05:07.421 
	 * @param timestamp
	 * @return
	 */
	private Instant parseDateTime(String timestamp) throws ParseException {
		try {
			return new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS").parse(timestamp).toInstant();
		}catch(ParseException e) {
			throw new ParseException(String.format("Timestamp Value: %s Error: %s", 
					timestamp, e.getMessage()), 
					e.getErrorOffset()
				);
		}
	}
	
	/**
	 * Parse a Double
	 * @param data
	 * @param field
	 * @return
	 * @throws NumberFormatException
	 */
	private double parseDouble(String[] data, AlertField field) throws NumberFormatException
	{
		int index = field.ordinal();
		try {
			return Double.parseDouble(data[index]);
		}catch(NumberFormatException e) {
			throw new NumberFormatException(String.format("Field: %s Value: %s was not a number!", 
					field, data[index])
				);
		}
	}

	/**
	 * Parse Integer
	 * @param data
	 * @param field
	 * @return
	 * @throws NumberFormatException
	 */
	private int parseInt(String[] data, AlertField field) throws NumberFormatException
	{
		int index = field.ordinal();
		try {
			return Integer.parseInt(data[index]);
		}catch(NumberFormatException e) {
			throw new NumberFormatException(String.format("Field: %s Value: %s was not a number!", 
					field, data[index])
				);
		}
	}
	
}
