package com.systolic.bdp.dto;

import java.time.Instant;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@JsonPropertyOrder({"satelliteId", "severity", "component","timestamp"})
public class SatelliteAlertDto {

	public static final String RED_HIGH="RED HIGH";
	public static final String RED_LOW="RED LOW";

	public static final String BATTERY="BATT";
	public static final String TERMOSTAT="TSTAT";
	
	/**
	 * The Satellite Reference
	 */
	int satelliteId;

	/**
	 * Level of severity
	 * @value RED HIGH a Thermostat is too high
	 * @value RED LOW a Battery is too low
	 */
	String severity;
	
	/**
	 * Type of component
	 * @value TSTAT a Thermostat
	 * @value BATT a Battery
	 */
	String component;

	/**
	 * format: 2018-01-01T23:01:09.521Z
	 */
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "UTC")
	Instant timestamp;

	/** 
	 * Only used for processing.
	 */
	@JsonIgnore
	int count;
}
